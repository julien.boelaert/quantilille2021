## QuantiLille 2021 - Analyse Textuelle 
## Julien Boelaert
## TD 2 - Expressions régulières

## Ce script est encodé en UTF-8 ; si les accents ne s'affichent pas bien, 
## utilisez dans Rstudio le menu Fichier -> Réouvrir avec encodage... 
## et choisissez UTF-8.

#############
## Préliminaires : environnement, bibliothèques
#############

rm(list = ls()) # Pour vider l'environnement

setwd("~/playground/") # à adapter au chemin sur votre ordinateur


#############
## Exemples utiles : caractères unicode, regex approximatif
#############

## Caractères unicode (signes diacritiques de toutes langues) : 
## https://www.regular-expressions.info/unicode.html
grep("\\p{L}", c("àéï", "...", "aei"), perl = T)
grep("\\w", c("àéï", "...", "aei"), perl = T)

grep("\\p{Ll}", c("ÂÉÖ", "...", "àëî"), perl = T)
grep("\\p{Lu}", c("ÂÉÖ", "...", "àëî"), perl = T)
grep("\\w", c("ÂÉÖ", "...", "àëî"), perl = T)

## Regex approximatif : agrep
mitterr <- c("Mitterrand", "Miterrand", "Miterand", "Mittérand", "mittérand")
agrep("Mitterrand", mitterr, value = T)
agrep("Mitterrand", mitterr, value = T, max.distance = 1)
agrep("Mitterrand", mitterr, value = T, max.distance = 2, ignore.case = TRUE)


#############
## Exercice : résultat d'extraction depuis le site Le Monde (lemonde.csv)
## Consigne : 
## 1 - Extraire les titres mentionnant Macron, en un vecteur (avec grep)
## 2 - Construire une data.frame contenant tous les titres en lignes, avec des
## colonnes pour indiquer la présence des mots "régionales", "culture", "Macron"
## (avec grepl)
## 3 - A partir de la colonne url, créer une nouvelle variable pour déterminer
## s'il s'agit d'un article, d'une vidéo ou autre (avec gsub)
## 4 - Nettoyer les titres d'articles (avec gsub)
#############

lem <- read.csv("lemonde.csv")

## 1 - Extraire les titres mentionnant Macron, en un vecteur (avec grep)
titres.macron <- grep("Macron", ignore.case = TRUE, value = TRUE, lem$titre)

## 2 - Construire une data.frame contenant tous les titres en lignes, avec des
## colonnes pour indiquer la présence des mots "régionales", "culture", "Macron"
## (avec grepl)
titres.tab <- data.frame(
  titre = lem$titre, 
  regionales = grepl("\\brégionales\\b", ignore.case = TRUE, lem$titre), 
  culture = grepl("\\bculture\\b", ignore.case = TRUE, lem$titre), 
  macron = grepl("\\bMacron", ignore.case = TRUE, lem$titre))

## 3 - A partir de la colonne url, créer une nouvelle variable pour déterminer
## s'il s'agit d'un article, d'une vidéo ou autre (avec gsub)
lem$type <- gsub("^.*?/(.*?)/2.*$", "\\1", lem$url)

## 4 - Nettoyer les titres d'articles (avec gsub)
lem$titre2 <- gsub("^\\s+|\\s+$", "", lem$titre)
lem$titre2 <- gsub("Article réservé .*? abonnés", "", lem$titre2)
lem$titre2 <- gsub("\n", " ", lem$titre2)
lem$titre2 <- gsub("\\s+", " ", lem$titre2)
lem$titre2 <- gsub("^\\s+|\\s+$", "", lem$titre2)
lem$titre2 <- gsub("(\\p{Ll})(\\p{Lu})", "\\1 \\2", lem$titre2, perl = TRUE)



#############
## Exercice (difficile) : Prix nobel de physique (nobel-physique.csv)
## Consigne : Nettoyer les noms des récipiendaires du prix Nobel de physique
#############

nobels <- read.csv("nobel-physique.csv")
nob.phy <- nobels$physique

## Espaces à la fin
nob.phy <- gsub("\\s+$", "", nob.phy)
## Correction des "Mather, John C. John C. Mather ;Smoot, George George Smoot"
nob.phy <- gsub("(.*), +(.*) +\\2 +\\1", "\\2 \\1", nob.phy, perl = TRUE)
## Espace après ;
nob.phy <- gsub(";(\\p{L})", "; \\1", nob.phy, perl = TRUE)

## Correction des "Arthur AshkinGérard MourouDonna Strickland"
grep("\\p{Ll}\\p{Lu}", nob.phy, value = TRUE, perl = TRUE)
nob.phy <- gsub("(\\p{Ll})(\\p{Lu})", "\\1 ; \\2", nob.phy, perl = TRUE)
grep("Mc ; ", nob.phy, value = TRUE)
nob.phy <- gsub("Mc ; ", "Mc", nob.phy)

## A nouveau les espaces de fin
nob.phy <- gsub("\\s+", " ", nob.phy, perl = TRUE)
grep("\\s{2}", nob.phy, value = TRUE, perl = TRUE)
grep("\\p{Zs}{2}", nob.phy, value = TRUE, perl = TRUE)
